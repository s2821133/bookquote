package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getBookPrice(String isbn){
        HashMap<String, Double> list = new HashMap<>();
        list.put("1",10.0);
        list.put("2",45.0);
        list.put("3",20.0);
        list.put("4",35.0);
        list.put("5",50.0);
        if (list.containsKey(isbn)){
            return list.get(isbn);
        }
        else
            return 0;
    }
}